package com.example.servicerecyclerview.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface LocationDao {
    @Query("SELECT * FROM locations")
    fun getAllLocations(): List<Location>

    @Insert
    fun insertAll(locations: Location)
}