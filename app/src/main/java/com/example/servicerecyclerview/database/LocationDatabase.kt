package com.example.servicerecyclerview.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase



@Database(entities = [Location::class], version = 2, exportSchema = false)
abstract class LocationDatabase : RoomDatabase() {

    abstract fun locationDao(): LocationDao

    companion object {
        @Volatile
        private var instance: LocationDatabase? = null

        fun getInstance(context: Context): LocationDatabase {
            return instance ?: synchronized(this) {
                return@synchronized instance ?: Room
                        .databaseBuilder(context.applicationContext, LocationDatabase::class.java, "sd_db")
                        .fallbackToDestructiveMigration().allowMainThreadQueries()
                        .build().apply {
                            instance = this
                        }
            }
        }
    }
}
