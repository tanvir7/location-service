package com.example.servicerecyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.servicerecyclerview.Hobby
import com.example.servicerecyclerview.R

class RecyclerAdapter(val context: Context, val hobbies: List<Hobby>) : RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>() {


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var latitudeId: TextView
        fun setData(hobby: Hobby?, pos: Int) {
            latitudeId = itemView.findViewById(R.id.latitdeId)
            latitudeId.text = hobby!!.title
        }

//        lateinit var longitudeId: TextView
//        fun setData(hobby: Hobby?, pos: Int) {
//            longitudeId = itemView.findViewById(R.id.longitudeID)
//            longitudeId.text = hobby!!.title
//        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =  LayoutInflater.from(context).inflate(R.layout.list_item, parent, false)
        return MyViewHolder(view)

    }

    override fun getItemCount(): Int {
        return hobbies.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val hobby = hobbies[position]
        holder.setData(hobby, position)
    }


}